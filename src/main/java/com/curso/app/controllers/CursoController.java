package com.curso.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.curso.app.models.Curso;
import com.curso.app.repository.CursoRepository;

@Controller
public class CursoController {
	
	@Autowired
	private CursoRepository er;
	
	@RequestMapping(value="/cadastrarCurso", method=RequestMethod.GET)
	public String form() {
		return "curso/formCurso";
	}
	
	@RequestMapping(value="/cadastrarCurso", method=RequestMethod.POST)
	public String form(Curso cursos) {
		er.save(cursos);
		return "redirect:/cadastrarCurso";
	}
	

}
