package com.curso.app.repository;

import org.springframework.data.repository.CrudRepository;

import com.curso.app.models.Curso;

public interface CursoRepository extends CrudRepository<Curso, String>{

}
