package com.curso.app.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Curso implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long codigo;
	
	private String NomeCurso;
	private String Coordenador;
	private String Turno;
	private String SemestresDuracao;
	
	
	public long getCodigo() {
		return codigo;
	}
	public void setCodigo(long codigo) {
		this.codigo = codigo;
	}
	
	public String getNomeCurso() {
		return NomeCurso;
	}
	public void setNomeCurso(String nomeCurso) {
		NomeCurso = nomeCurso;
	}
	public String getCoordenador() {
		return Coordenador;
	}
	public void setCoordenador(String coordenador) {
		Coordenador = coordenador;
	}
	public String getTurno() {
		return Turno;
	}
	public void setTurno(String turno) {
		Turno = turno;
	}
	public String getSemestresDuracao() {
		return SemestresDuracao;
	}
	public void setSemestresDuracao(String semestresDuracao) {
		SemestresDuracao = semestresDuracao;
	}
}
